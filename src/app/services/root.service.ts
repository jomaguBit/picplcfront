import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { environmentP } from '../../environments/environment.prod';
import { Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

const API_SALES=environmentP.browseURL;

@Injectable()
export class RootService {

  flagVentas:number = 1;
  heroeServ:any[] = [];

  constructor( private http:HttpClient,
               public router: Router ) { }


  public browseVenta( firstD:String, lastD:String,
                      canal:String, ofic:String,
                      lotId:string, status:string  ){
    /*  console.log('SOY CONSOLE SERVER!!: ');
      console.log('fech ini: ' + firstD + ',');
      console.log('fech fin: ' + lastD  + ',');
      console.log('canal: ' + canal  + ',');
      console.log('ofic: ' + ofic  + ',');
      console.log('lot: ' + lotId  + ',');
      console.log('estado: ' + status  + '');*/

      let templateDate:string = "" + firstD;
      let templateDate2:string = "" + lastD;
      let varCanal:string = "" + canal;
      let varOfic:string = "" + ofic;
      let varLotId:string = "" + lotId;
      let varStatus:string = "" + status;

      localStorage.setItem('s_firstD', templateDate);
      localStorage.setItem('s_lastD', templateDate2);
      localStorage.setItem('s_idCanal', varCanal);
      localStorage.setItem('s_idOficina', varOfic);
      localStorage.setItem('s_idLote', varLotId);
      localStorage.setItem('s_idStatus', varStatus);

      let body = {
        order:"",
        startDate:templateDate,
        endDate:templateDate2,
        officceId:varOfic,
        channelId:varCanal,
        lotId:varLotId,
        status:varStatus
      };

      let headers = new HttpHeaders({
        'Accept':'application/json',
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      });

      return this.http.post( API_SALES, body, { headers }  )
      .pipe(
        map((res:any) => {
          console.log(res);
          this.flagVentas = 2;
          localStorage.setItem('s_records', (res).records);
          this.heroeServ = (res).salesResult;
          return (res).salesResult;
          //this.router.navigate(['/sales']);
          //FFILTRO DEL MODULO VENTAS: ESTE ES EL METODO DE BUSQUEDA DE ORDENESS DE VENTAS PARAMETRO MAVIDOS VACIOS O CON CONTENIDO
        })

      );
  }


  showAlert(message){
    if(window.confirm(message)){
      this.router.navigate(['/crmodulo']);
      } else{
      this.router.navigate(['/crmodulo']);
    }
  }

}
